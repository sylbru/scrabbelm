module Tests exposing (..)

import Expect
import Test exposing (..)
import Tile



-- Check out https://package.elm-lang.org/packages/elm-explorations/test/latest to learn more about testing in Elm!


all : Test
all =
    describe "Tiles"
        [ test "Tile to String" <|
            \_ ->
                Expect.equal (Tile.tileToString Tile.T) "T"
        , test "String to Tile" <|
            \_ ->
                Expect.equal (Tile.stringToTile "T") Tile.T
        , test "String to Tile should accept lowercase letters" <|
            \_ ->
                Expect.equal (Tile.stringToTile "t") Tile.T
        , test "String to Tile should default to wildcard" <|
            \_ ->
                Expect.equal (Tile.stringToTile "*") Tile.Wildcard
        , test "String to Tile should ignore diacritics" <|
            \_ ->
                Expect.equal (Tile.stringToTile "é") Tile.E
        , test "English tile distribution should have 100 tiles" <|
            \_ ->
                Expect.equal (List.length Tile.englishDistribution) 100
        , test "French tile distribution should have 102 tiles" <|
            \_ ->
                Expect.equal (List.length Tile.frenchDistribution) 102
        ]
