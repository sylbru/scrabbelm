module Board exposing (Board, Cell, Position, classicBoard, getCell, setCell, updateCell)

import Array exposing (Array)
import Bonus exposing (Bonus(..))
import Tile exposing (Tile)


type alias Board =
    Array (Array Cell)


type alias Cell =
    { bonus : Bonus
    , tile : Maybe Tile
    , position : Position
    , input : Bool
    }


type alias Position =
    ( Int, Int )


classicBoard : Board
classicBoard =
    let
        halfBoard =
            Array.initialize 8 classicBoardRow
                |> Array.toList

        restBoard =
            halfBoard
                |> List.reverse
                -- Ignore first row, already at the end of the first half
                |> List.tail
                -- Tail returns a maybe
                |> Maybe.withDefault []
    in
    List.append halfBoard restBoard
        |> Array.fromList
        |> numberCellsInBoard


numberCellsInBoard : Board -> Board
numberCellsInBoard board =
    board
        |> Array.indexedMap
            (\row line -> line |> Array.indexedMap (numberCell row))


numberCell : Int -> Int -> Cell -> Cell
numberCell row column cell =
    { cell | position = ( row, column ) }


classicBoardRow : Int -> Array Cell
classicBoardRow row =
    let
        list =
            case row of
                0 ->
                    [ WordTriple, None, None, LetterDouble, None, None, None, WordTriple ]

                1 ->
                    [ None, WordDouble, None, None, None, LetterTriple, None, None ]

                2 ->
                    [ None, None, WordDouble, None, None, None, LetterDouble, None ]

                3 ->
                    [ LetterDouble, None, None, WordDouble, None, None, None, LetterDouble ]

                4 ->
                    [ None, None, None, None, WordDouble, None, None, None ]

                5 ->
                    [ None, LetterTriple, None, None, None, LetterTriple, None, None ]

                6 ->
                    [ None, None, LetterDouble, None, None, None, LetterDouble, None ]

                7 ->
                    [ WordTriple, None, None, LetterDouble, None, None, None, WordDouble ]

                _ ->
                    List.repeat 8 None
    in
    emptyRowWithBonuses list


emptyRowWithBonuses : List Bonus -> Array Cell
emptyRowWithBonuses bonuses =
    let
        halfRow =
            List.map emptyCellWithBonus bonuses

        restRow =
            halfRow
                |> List.reverse
                -- Ignore first cell, already at the end of the first half
                |> List.tail
                -- Tail returns a maybe
                |> Maybe.withDefault []

        fullRow =
            List.append halfRow restRow
    in
    fullRow
        |> Array.fromList


emptyCellWithBonus : Bonus -> Cell
emptyCellWithBonus bonus =
    { bonus = bonus
    , tile = Nothing
    , position = ( -1, -1 )
    , input = False
    }


updateCell : Maybe Tile.Tile -> Position -> Board -> Board
updateCell tile position board =
    let
        cell =
            getCell position board

        changedCell =
            { cell | tile = tile }
    in
    setCell changedCell position board


setCell : Cell -> Position -> Board -> Board
setCell cell ( row, column ) board =
    let
        line =
            Array.get row board
                |> Maybe.withDefault (emptyCellWithBonus None |> Array.repeat 15)

        changedLine =
            Array.set column cell line
    in
    Array.set row changedLine board


getCell : Position -> Board -> Cell
getCell ( row, column ) board =
    let
        line =
            Array.get row board
                |> Maybe.withDefault (emptyCellWithBonus None |> Array.repeat 15)
    in
    Array.get column line
        |> Maybe.withDefault (emptyCellWithBonus None)
