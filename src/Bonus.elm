module Bonus exposing (Bonus(..))


type Bonus
    = None
    | LetterDouble
    | LetterTriple
    | WordDouble
    | WordTriple
