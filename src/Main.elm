module Main exposing (..)

import Array exposing (Array)
import Board
import Bonus
import Browser
import Html exposing (Html, div, h1, img, main_, text)
import Html.Attributes exposing (class, src, style)
import Html.Events exposing (onClick, onInput)
import Random
import Random.List
import Tile



---- MODEL ----


type alias Model =
    { players : Players
    , board : Board.Board
    , bag : Tile.Bag
    , currentPlayer : CurrentPlayer
    }


type alias Players =
    List Player


type alias CurrentPlayer =
    Int


type alias Player =
    { name : String
    , tiles : List Tile.Tile
    }


initialModel : Model
initialModel =
    { players = [ newPlayer "Alice", newPlayer "Bob", newPlayer "Charlie" ]
    , board = testBoard
    , bag = Tile.englishDistribution
    , currentPlayer = 0
    }


newPlayer : String -> Player
newPlayer name =
    Player name []


testBoard : Board.Board
testBoard =
    Board.classicBoard
        |> Board.updateCell (Just Tile.S) ( 7, 7 )
        |> Board.updateCell (Just Tile.C) ( 7, 8 )
        |> Board.updateCell (Just Tile.R) ( 7, 9 )
        |> Board.updateCell (Just Tile.A) ( 7, 10 )
        |> Board.updateCell (Just Tile.B) ( 7, 11 )
        |> Board.updateCell (Just Tile.Wildcard) ( 7, 12 )
        |> Board.updateCell (Just Tile.L) ( 7, 13 )
        |> Board.updateCell (Just Tile.E) ( 7, 14 )


init : ( Model, Cmd Msg )
init =
    update (ShuffleNTimes 42 initialModel.bag) initialModel



---- UPDATE ----


type Msg
    = ShuffleNTimes Int Tile.Bag
    | UpdateBag Tile.Bag
    | DrawTiles CurrentPlayer
    | InputTile Board.Position
    | ReceivedInputTile Board.Position String



--| PlaceTile Board.Position Tile.Tile


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ShuffleNTimes n shuffledBag ->
            if n <= 0 then
                ( { model | bag = shuffledBag }, Cmd.none )

            else
                ( model, Random.generate (ShuffleNTimes (n - 1)) (Random.List.shuffle shuffledBag) )

        DrawTiles currentPlayer ->
            let
                ( newBag, newPlayers ) =
                    drawTiles currentPlayer model.bag model.players
            in
            ( { model | bag = newBag, players = newPlayers }, Cmd.none )

        InputTile position ->
            let
                cell =
                    Board.getCell position model.board

                inputCell =
                    { cell | input = True }
            in
            ( { model | board = Board.setCell inputCell position model.board }, Cmd.none )

        ReceivedInputTile position string ->
            ( { model | board = Board.updateCell (Just (Tile.stringToTile string)) position model.board }, Cmd.none )


drawTiles : CurrentPlayer -> Tile.Bag -> Players -> ( Tile.Bag, Players )
drawTiles currentPlayerIndex bag players =
    let
        currentPlayer =
            players |> Array.fromList |> Array.get currentPlayerIndex

        ( newBag, updatedPlayer ) =
            case currentPlayer of
                Just player ->
                    drawTilesForPlayer bag player

                Nothing ->
                    -- currentPlayerIndex is out of bounds
                    ( bag, newPlayer "" )

        newPlayers =
            case currentPlayer of
                Just player ->
                    players |> Array.fromList |> Array.set currentPlayerIndex updatedPlayer |> Array.toList

                Nothing ->
                    players
    in
    ( newBag, newPlayers )


drawTilesForPlayer : Tile.Bag -> Player -> ( Tile.Bag, Player )
drawTilesForPlayer bag player =
    let
        tilesNeeded =
            maxTilesPerPlayer - List.length player.tiles

        newTiles =
            List.take tilesNeeded bag

        newBag =
            List.drop tilesNeeded bag

        _ =
            Debug.log (String.fromInt tilesNeeded)
    in
    ( newBag, { player | tiles = List.append player.tiles newTiles } )


maxTilesPerPlayer : Int
maxTilesPerPlayer =
    7


getCurrentPlayer : Model -> Maybe Player
getCurrentPlayer model =
    Array.get model.currentPlayer (Array.fromList model.players)



---- VIEW ----


view : Model -> Html Msg
view model =
    let
        currentPlayer =
            getCurrentPlayer model

        currentTiles =
            case currentPlayer of
                Just player ->
                    player.tiles

                Nothing ->
                    []
    in
    main_ [ class "main" ]
        [ div [ class "board" ] (viewBoard model.board)
        , div [ class "info" ] (viewInfo model)
        , viewRack currentTiles
        ]


viewInfo : Model -> List (Html Msg)
viewInfo model =
    let
        tileCount =
            model.bag |> List.length |> String.fromInt

        tileList =
            model.bag |> List.map Tile.tileToChar |> List.map String.fromChar |> String.join ", "
    in
    [ div [ class "players" ] (List.indexedMap (viewInfoPlayer model.currentPlayer) model.players)
    , div []
        [ text
            ("Bag contains "
                ++ tileCount
                ++ " tile"
                ++ (if List.length model.bag > 1 then
                        "s"

                    else
                        ""
                   )
             -- ++ " ("
             -- ++ tileList
             -- ++ ")"
            )
        ]
    ]


viewInfoPlayer : CurrentPlayer -> CurrentPlayer -> Player -> Html Msg
viewInfoPlayer currentPlayer indexPlayer player =
    let
        isPlaying : Bool
        isPlaying =
            currentPlayer == indexPlayer

        status =
            if isPlaying then
                [ text "(playing)"
                , Html.button [ onClick (DrawTiles currentPlayer) ] [ text "Draw tiles" ]
                ]

            else
                []
    in
    div [ class "player" ]
        [ div [ class "player__name" ] [ text player.name ]
        , div [ class "player__status" ] status
        , div [ class "player__tiles" ] [ text <| String.fromInt (List.length player.tiles) ]
        ]


viewRack : List Tile.Tile -> Html Msg
viewRack tiles =
    div [ class "rack" ]
        (List.map (\tile -> div [ class "tile rack__tile" ] [ tile |> Tile.tileToChar |> String.fromChar |> text ]) tiles)


viewBoard : Board.Board -> List (Html Msg)
viewBoard board =
    Array.map (\line -> Array.map viewCell line) board
        |> Array.foldr Array.append Array.empty
        -- layout currently has to be consistent with the board dimensions (15 rows/15 columns)
        |> Array.toList


viewCell : Board.Cell -> Html Msg
viewCell cell =
    let
        classes =
            case cell.bonus of
                Bonus.LetterDouble ->
                    [ class "cell--letter-double" ]

                Bonus.LetterTriple ->
                    [ class "cell--letter-triple" ]

                Bonus.WordDouble ->
                    [ class "cell--word-double" ]

                Bonus.WordTriple ->
                    [ class "cell--word-triple" ]

                Bonus.None ->
                    []

        textPosition =
            (cell.position |> Tuple.first |> String.fromInt) ++ "," ++ (cell.position |> Tuple.second |> String.fromInt)

        contents =
            case cell.tile of
                Just letter ->
                    [ div
                        [ class "tile" ]
                        [ text (Tile.tileToChar letter |> Char.toUpper |> String.fromChar) ]
                    ]

                _ ->
                    if cell.input then
                        [ Html.input
                            [ class "cell__input"
                            , Html.Attributes.maxlength 1
                            , onInput (\string -> ReceivedInputTile cell.position string)
                            ]
                            []
                        ]

                    else
                        []

        events =
            case cell.tile of
                Just letter ->
                    []

                Nothing ->
                    [ onClick (InputTile cell.position) ]

        attributes =
            List.append (class "cell" :: classes) events
    in
    div attributes contents



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
