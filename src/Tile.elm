module Tile exposing (Bag, Tile(..), englishDistribution, frenchDistribution, stringToTile, tileToChar, tileToString)

import Dict exposing (Dict)


type Tile
    = Wildcard
    | A
    | B
    | C
    | D
    | E
    | F
    | G
    | H
    | I
    | J
    | K
    | L
    | M
    | N
    | O
    | P
    | Q
    | R
    | S
    | T
    | U
    | V
    | W
    | X
    | Y
    | Z


type alias Bag =
    List Tile


englishDistribution : Bag
englishDistribution =
    makeBag
        [ ( E, 12 )
        , ( A, 9 )
        , ( I, 9 )
        , ( O, 8 )
        , ( N, 6 )
        , ( R, 6 )
        , ( T, 6 )
        , ( L, 4 )
        , ( S, 4 )
        , ( U, 4 )
        , ( D, 4 )
        , ( G, 3 )
        , ( B, 2 )
        , ( C, 2 )
        , ( M, 2 )
        , ( P, 2 )
        , ( F, 2 )
        , ( H, 2 )
        , ( V, 2 )
        , ( W, 2 )
        , ( Y, 2 )
        , ( K, 1 )
        , ( J, 1 )
        , ( X, 1 )
        , ( Q, 1 )
        , ( Z, 1 )
        , ( Wildcard, 2 )
        ]


frenchDistribution : Bag
frenchDistribution =
    makeBag
        [ ( E, 15 )
        , ( A, 9 )
        , ( I, 8 )
        , ( N, 6 )
        , ( O, 6 )
        , ( R, 6 )
        , ( S, 6 )
        , ( T, 6 )
        , ( U, 6 )
        , ( L, 5 )
        , ( D, 3 )
        , ( M, 3 )
        , ( G, 2 )
        , ( B, 2 )
        , ( C, 2 )
        , ( P, 2 )
        , ( F, 2 )
        , ( H, 2 )
        , ( V, 2 )
        , ( J, 1 )
        , ( Q, 1 )
        , ( K, 1 )
        , ( W, 1 )
        , ( X, 1 )
        , ( Y, 1 )
        , ( Z, 1 )
        , ( Wildcard, 2 )
        ]


makeBag : List ( Tile, Int ) -> Bag
makeBag distribution =
    let
        go distrib acc =
            case distrib of
                ( tile, count ) :: rest ->
                    List.repeat count tile
                        |> List.append acc
                        |> go rest

                [] ->
                    acc
    in
    go distribution []


tileToChar : Tile -> Char
tileToChar tile =
    case tile of
        Wildcard ->
            ' '

        A ->
            'A'

        B ->
            'B'

        C ->
            'C'

        D ->
            'D'

        E ->
            'E'

        F ->
            'F'

        G ->
            'G'

        H ->
            'H'

        I ->
            'I'

        J ->
            'J'

        K ->
            'K'

        L ->
            'L'

        M ->
            'M'

        N ->
            'N'

        O ->
            'O'

        P ->
            'P'

        Q ->
            'Q'

        R ->
            'R'

        S ->
            'S'

        T ->
            'T'

        U ->
            'U'

        V ->
            'V'

        W ->
            'W'

        X ->
            'X'

        Y ->
            'Y'

        Z ->
            'Z'


normalizeChar : Char -> Char
normalizeChar char =
    case Dict.get (Char.toLower char) replacements of
        Just replacement ->
            replacement

        Nothing ->
            Char.toUpper char


replacements : Dict Char Char
replacements =
    [ ( 'é', 'E' )
    , ( 'è', 'E' )
    , ( 'ë', 'E' )
    , ( 'ê', 'E' )
    , ( 'à', 'A' )
    , ( 'á', 'A' )
    , ( 'ä', 'A' )
    , ( 'â', 'A' )
    , ( 'ì', 'I' )
    , ( 'í', 'I' )
    , ( 'ï', 'I' )
    , ( 'î', 'I' )
    , ( 'ò', 'O' )
    , ( 'ó', 'O' )
    , ( 'ö', 'O' )
    , ( 'ô', 'O' )
    , ( 'ù', 'U' )
    , ( 'ú', 'U' )
    , ( 'ü', 'U' )
    , ( 'û', 'U' )
    , ( 'ç', 'C' )
    ]
        |> Dict.fromList


charToTile : Char -> Tile
charToTile char =
    let
        normalizedChar =
            normalizeChar char
    in
    case normalizedChar of
        'A' ->
            A

        'B' ->
            B

        'C' ->
            C

        'D' ->
            D

        'E' ->
            E

        'F' ->
            F

        'G' ->
            G

        'H' ->
            H

        'I' ->
            I

        'J' ->
            J

        'K' ->
            K

        'L' ->
            L

        'M' ->
            M

        'N' ->
            N

        'O' ->
            O

        'P' ->
            P

        'Q' ->
            Q

        'R' ->
            R

        'S' ->
            S

        'T' ->
            T

        'U' ->
            U

        'V' ->
            V

        'W' ->
            W

        'X' ->
            X

        'Y' ->
            Y

        'Z' ->
            Z

        _ ->
            Wildcard


tileToString : Tile -> String
tileToString tile =
    tile |> tileToChar |> String.fromChar


stringToTile : String -> Tile
stringToTile string =
    string |> String.uncons |> Maybe.withDefault ( ' ', "" ) |> Tuple.first |> charToTile
